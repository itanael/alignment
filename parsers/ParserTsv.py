# -*- coding: utf-8 -*-
import os

class parserTsv:

    def __init__(self):
        self.dict_sentences = {}

    #Creation of dictionnary from a file
    def parserTSVtoDict(self,file_path):
        sentence = self.initSentence()
        id_line = 1
        lines = file_path.readlines()
        id_phrase = ""

        for line in lines:
            line = line.strip()
            if not bool(line):
                sentence['sentence'] = self.cleanText(sentence['sentence'])
                self.dict_sentences[id_phrase] = sentence
                sentence = self.initSentence()
                id_line = 1
                
            else:
                if line.startswith("# source_sent_id ="):
                    sentence['source'] = self.getValue(line)
                    sentence['id_source'] = self.getIdSource(line)
                    id_phrase = self.getId(line)
                elif not line.startswith("#"):
                    self.getWordsAndAnnotations(line, sentence['words'], id_phrase, sentence['sentence'])
                    sentence['sentence'] += self.getText(line)
                    if self.checkEntry(line) is None:
                        continue
                    
                    self.grepAnnotations(
                        line,
                        id_phrase,
                        sentence['annotations'],
                        sentence['sentence']
                    )
                    id_line += 1

    def initSentence(self):
        return {
            'sentence' : [],
            'annotations': {},
            'words': {},
            'source':''
        }

    def getText(self, line):
        columns = line.split("\t")
        return [columns[1]]


    def getIdSource(self, line):
        if 'sequoia' in line:
            return 1
        elif 'fr-ud' in line :
            return 2
        elif 'fr_partut-ud' in line:
            return 3
        elif 'fr_pud-ud' in line:
            return 4
        else:
            return 0

    '''
    Transform disjoint determinants 
    into joined version
    -> "aux" == "à les"
    '''
    def cleanText(self, text):
        sentence = " ".join(text)
        transformations = {
            "Au à le" : "Au ",
            "Aux à les " : "Aux ",
            "Des de les " : "Des ",
            "Du de le ": "Du ",
            "Du de la ": "Du ",
            "au à le " : "au ",
            "aux à les " : "aux ",
            "des de les " : "des ",
            "du de le ": "du ",
            "du de la ": "du "
        }
        for origin, new in transformations.items():
            sentence = sentence.replace(origin,new)
            
        return sentence.split(" ")
                        

    #Get value from a line
    def getValue(self, line):
        index = line.find("=")

        if index == -1:
            return ""

        return line[index + 1:].strip()

    #Formalize an ID for an entry
    def getId(self, line):
        line = line.split(" ")
        corpus  = line[4].split("/")
        corpus = corpus[1].replace("_","-").replace(".","_").split("_")
        id_phrase = corpus[0]
        id_phrase += "-" + line[5]
        return id_phrase.replace("sequoia","seq").upper()

    #Check if there is annotation in line
    def checkEntry(self, line):
        columns = line.split("\t")

        if columns[10] == '*':
            return 

        return {
            'form': [columns[1]],
            'lemma': [columns[2]],
            'pos': columns[3]
        }

    #Get the words list and their annotations
    def getWordsAndAnnotations(self, line, words, phraseID, sentence):
        columns = line.split("\t")
        listdet = ["au","aux","des","du","à","de"]    
        listdetAvoid = ["les","le","la"]

        if "-" in columns[0]:
            splitcolumns = columns[0].split('-')
            columns[0] = splitcolumns[0]

        #If no annotation
        if columns[10] == '*':
            if not words.get(columns[0]):
                if len(sentence) > 1:
                    if columns[1].lower() in listdetAvoid and sentence[-1].lower() in listdet and sentence[-2].lower() in listdet:
                        return

                words[columns[0]] = {
                    'text': columns[1],
                    'grammar' : self.splitGrammarColumn(columns[5]),
                    'deprel' : [columns[7]],
                    'annotationID' : {}
                }

        else:
            '''
            Get the Parseme-mwe column
            Analyze column's length
            Depending on case, values are dispatched
            Examples :
                '1:LVCFULL;2:VID'
                '1;2;3'
                '1:LVCFULL'
            '''
            elements = columns[10].split(';')

            if len(sentence) > 1:
                if columns[1].lower() in listdetAvoid and sentence[-1].lower() in listdet and sentence[-2].lower() in listdet:
                    return

            words[columns[0]] = {
                'text': columns[1],
                'grammar' : self.splitGrammarColumn(columns[5]),
                'deprel' : [columns[7]],
                'annotationID' : {}
            }
            
            for el in elements:
                annotation = el.split(':')
                annot_id = annotation[0]

                #If it's the annotation's beginning
                if len(annotation) != 1:
                    words[columns[0]]['annotationID'][annot_id] = str(phraseID) + '-' + str(annot_id)

                #If it's not the annotation's beginning
                else: 

                    #If position already exist
                    if words.get(columns[0]):
                        #Add annotation's id
                        words[columns[0]]['annotationID'][annot_id] = str(phraseID)  + '-' + str(annot_id)

                    else:
                        '''
                        If the sentence's last word is not in the list of determinants
                        But the previous word is in
                        And the current word should be avoided
                        We add it in words list
                        Exemple : "parler de les " 
                        '''
                        if columns[1].lower() in listdetAvoid and sentence[-1].lower() in listdet and sentence[-2].lower() not in listdet:
                            words[columns[0]]['annotationID'][annot_id] = str(phraseID) + '-' + str(annot_id)
                        # If it's lambda word
                        #We can add it in words list
                        if columns[1] not in listdetAvoid:
                            words[columns[0]]['annotationID'][annot_id] = str(phraseID) + '-' + str(annot_id)
                
    #Get grammar info for a word                 
    def splitGrammarColumn(self, column):
        if column == '_':
            return
        elements = column.split('|')
        informations = {}
        for element in elements :
            el = element.split('=')
            informations[el[0]] = el[1]
        
        return informations
             
                
    #Grep the annotations from the entry
    def grepAnnotations(self, line, phraseID, annotations, sentence):
        columns = line.split("\t")
        listdet = ["au","aux","des","du","à","de"]
        listdetAvoid = ["les","le","la"]

        if columns[10] == '*':
            return

        '''
        Get the Parseme-mwe column
        Analyze column's length
        Depending on case, values are dispatched
        Examples :
            '1:LVCFULL;2:VID'
            '1;2;3'
            '1:LVCFULL'
        '''

        elements = columns[10].split(';')

        for el in elements:
            annotation = el.split(':')
            annot_id = annotation[0]
            annotationID = str(phraseID) + '-' + str(annot_id)

            #It's not annotation's beginning
            if len(annotation) == 1:
                if annot_id not in annotations:
                    continue
                annot = annotations[str(annot_id)]
                annot["form"] += [columns[1]]
                annot["lemma"] += [columns[2]]
                annot["pos"] += [columns[3]]
            
            #It's annotation's beginning
            else:
                annotations[str(annot_id)] = {
                    "id": annotationID,
                    "type":  annotation[1],
                    "form" : [columns[1]],
                    "lemma": [columns[2]],
                    "pos" : [columns[3]],
                    "positions" : [],
                    "heads": []
                }
                annot = annotations[str(annot_id)]

            #Add heads for alignment
            if columns[3] in ['VERB', 'NOUN', 'ADJ']:
                annot["heads"] += [columns[2].lower()]
            
            #Depending on case, add positions for annotations
            if sentence[-1] in listdet and columns[1] not in listdetAvoid:
                annot["positions"] += [columns[0]]
            if sentence[-1] not in listdet and columns[1] not in listdetAvoid:
                annot["positions"] += [columns[0]]
                
    # Generalize processus on multiple files
    def generalizationTSVFiles(self, directory):
            file_list = os.listdir(directory)
            for i in file_list:
                if i.endswith('.cupt'):
                    if os.path.getsize(directory + i) > 0 :
                        with open(directory + i, 'r') as f:
                            self.parserTSVtoDict(f)
            return self.dict_sentences
