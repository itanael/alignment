# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import json
import os
import sys
import re

class parserXML:

    def __init__(self):
        self.dict = {}
        self.encoding = sys.stdout.encoding or 'utf-8'

    def parseXmlWithEtree(self, filePath):
        tree = ET.parse(filePath)
        root = tree.getroot()
        return root

    def initEntry(self, file):
        return {
            "id": "",
            "source_file": file,
            "text": [],
            "type": "",
            "head": {
                "properties": {}
            },
            "complements": {},
            "arguments": {}
        }

    '''
        Formalize entries from xml
        Create dictionnaries object with entries
    '''
    def formatEntry(self, filePath, filename):
        root = self.parseXmlWithEtree(filePath)

        for child in root:
            entry = self.initEntry(filename)
            ep = re.sub(r"\(.*\)","",child.attrib["id"]).split("_")
            
            entry["id"] = "-".join(ep[:2]) + "-" + ep[-1]

            if ep[len(ep) - 1].isdigit():
                ep = ep[:len(ep) - 1]

            entry["type"] = ep[1]
            entry["text"] += ep[2:]

            entry["text"] = self.format_text(entry["text"]) 

            #Head
            head_node = child.find("head")

            if head_node is None :
                continue

            head = entry["head"]

            #Get head's informations
            head_component = head_node.find(".//component")
            #If there is component in head's node
            if head_component is not None :
                for element in head_component :
                    head[element.tag] = element.text

            if "word" not in entry["head"] :
                continue
    
            '''
            Get properties like :
                -- ppv : past participle
                -- impersonal verb
                -- use 'en' or 'y'
            '''
            properties = head_node.findall(".//property")
            head_props = head["properties"]

            for prop in properties :
                for name in prop.attrib :
                    if name.startswith("ppv-") and prop.attrib[name] == "True":
                        head_props["ppv"] = name[4:]                       
                    else :
                        head_props[name] = (prop.attrib[name] == "True")

            #Realizations
            '''
            Get realizations like:
                -- human
                -- not human
                -- cat
                -- mood
            '''
            frame_node = child.find("frame")
            no = 1
            if frame_node is not None:
                for constituent in frame_node:
                    realizations = constituent.findall(".//realizations")
                    constituent_components = constituent.findall('.//component')
                    
                    argument = entry["complements"][constituent.attrib["rid"]] = {
                        "words": [],
                        "pos" : [],
                        "props": {}
                    }                   

                    self.get_complement_word_pos(argument, constituent_components)
                    self.get_realization(argument, realizations)
            
            if 'ppv' in  head_props.keys() and 0 in entry['complements'].keys():
                entry['complements'][0]['props'][0]['ppv'] = head_props["ppv"] if head_props["ppv"] is not None else ''
                
            no = 1
            #Arguments
            words = ["lemma", "word", "subtype"]

            #Because there are errors in lexical files
            listDet = ["mon","ton","son","ma","ta","sa",
                       "mes","tes","ses","notre","votre",
                       "leur","nos","vos", "leurs", "un",
                       "une", "le", "la", "les"]

            component = frame_node.findall(".//component")

            if component is not None:
                for elements in component:
                    entry["arguments"][no] = {}
                    for element in elements:
                        if element.tag in words:
                            entry["arguments"][no]["word"] = element.text
                            if element.text in listDet:
                                entry["arguments"][no]["pos"] = "Det"
                        else:
                            entry["arguments"][no][element.tag] = element.text
                    no += 1
            
            if 'ppv' in  head_props.keys() and 0 in entry['arguments'].keys():
                entry['arguments'][0]['ppv'] = head_props["ppv"] if head_props["ppv"] is not None else ''
            

            head_word = head["word"]
            prefix = ""

            if type(head_word) == unicode:
                head_word = head_word.encode(self.encoding)

            synonyms = self.getSynonyms(head_word.lower())


            if len(synonyms) > 0:
                for word in synonyms:
                    if word.decode(self.encoding).encode(self.encoding).lower() not in self.dict.keys():
                        self.dict[word.decode(self.encoding).encode(self.encoding).lower()] = {}

            if head_word.lower() not in self.dict.keys():
                self.dict[head_word.lower()] = {}

            # Add prefix in head_word
            if "Neg" in head["properties"] and head["properties"]["Neg"] is not None :
                prefix += "ne"
            if "ppv" in head["properties"]:
                prefix += " " + head["properties"]["ppv"]
                prefix = prefix.strip()
            if prefix != "":
                text = " ".join(entry["text"])
                entry["text"] = text.replace(head_word, " " + prefix + " " + head_word).strip().split(" ")
            

            if len(synonyms) > 0 :
                for word in synonyms:
                    self.dict[word.decode(self.encoding).encode(self.encoding).lower()][entry["id"]] = entry

            self.dict[head_word.lower()][entry["id"]] = entry

    def format_text(self, text):
        txt = " ".join(text)
        if type(txt) == unicode:
            txt = txt.encode(self.encoding)

        txt = txt.replace("$","").lower()
        txt = re.sub(r'être.+?est','est', txt)
        txt = re.sub(r'être.+?sont','sont', txt)
        return txt.split(" ")
    

    def get_complement_word_pos(self, dict, constituent_components):
        for component in constituent_components:
            wordNode = component.find("word")
            posNode = component.find("pos")

            if wordNode is not None and wordNode.text is not None:
                dict["words"] += [wordNode.text]
            
            if posNode is not None and posNode.text is not None:
                dict["pos"] += [posNode.text]

    def get_realization(self, argument, realizations):
        no = 1
        for realization in realizations:
            for element in realization:
                argument['props'][no] = {}
                for key in element:
                    argument['props'][no][key.tag] = key.text      
                no += 1


    def getSynonyms(self, verb):
        dictSynonyms = {
            'faire': [
                'effectuer','entraîne','apporter','mener',
                'accomplir','élaborer','établir','causer',
                'créer','engendrer','exécuter','façonner',
                'fonder','former','instaurer','instituer',
                'foutre','opérer','pondre','préparer',
                'connaître','perpétrer','exercer','subir',
                'procéder','adresser','composer','dérouler'
                ],
            'être':[
                'comporte','présenter','soumettre','mettre',
                'accomplir','consister','devenir','faire',
                'réaliser','contenter'
            ],
            'présenter':['être'],
            'garder':['avoir'],
            'avoir':['garder'],
            'porter':['prêter'],
            'apparaître':['réapparaître'],
            'trouver':['tomber','être'],
            'effectuer':['faire','réaliser']
        }

        if verb.lower() in dictSynonyms.keys():
            return dictSynonyms[verb]
        return []


    def generalizationEntries(self, directory):
        filelist = os.listdir(directory)
        for filename in filelist:
            if not filename.endswith('.xml'):
                continue
            if not os.path.getsize(directory + filename) > 0 :
                continue

            with open(directory + filename, 'r') as f:
                self.formatEntry(f, filename)

        return self.dict

    
