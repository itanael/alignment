## Présentation ##

- Parser XML : Parser les fichiers de lg2parseme, transformation en dictionnaires python. (Source : Lexique-Grammaire de Maurice Gross)
- Parser TSV : Parser les fichiers français de sharedtask-data

- Alignement : Deux choix (prod et débugging) en fonction de ce qui est désiré

### Installation : ###

```bash
mkdir align
cd align

git clone https://github.com/MathieuConstant/lg2parseme-fr.git
git clone https://gitlab.com/parseme/sharedtask-data.git
git clone https://bitbucket.org/itanael/alignment.git

cd lg2parseme-fr
mkdir tmp
./lg2parseme-fr tmp
```

### Usage : ###

Depuis la racine :

```bash
  # pour la production
  python2.7 alignment/align.py

  # pour le debug
  python2.7 alignment/align.py --debug
```

### Ressources : ###

- [lg2parseme-fr (lexique)](https://github.com/MathieuConstant/lg2parseme-fr)
- [sharedtask-data (corpus)](https://gitlab.com/parseme/sharedtask-data)
