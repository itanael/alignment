#!/usr/bin/python
# coding=utf-8

from parsers.ParserTsv import parserTsv
from parsers.ParserXML import parserXML
from sets import Set
from fuzzywuzzy import fuzz

import argparse
import sys
import re
import csv
import os

'''
It's the class for alignment between corpus and lexical entries.
Version : Python 2.7
Glossary : 
    - pos : part of speech
Three methods :
    - Exact matching
    - Various matching (same elements but different order)
    - Semantic matching (similary syntax/words but different sense)
Two ways : production and debugging
'''

class Align:

    def __init__(self, debug=False):
        self.parserTsv = parserTsv()
        self.parserXML = parserXML()

        self.debug = debug

        self.parserTsv.generalizationTSVFiles('./sharedtask-data/1.1/FR/')
        self.XMLText = self.parserXML.generalizationEntries('./lg2parseme-fr/tmp/')
        self.encoding = sys.stdout.encoding or 'utf-8'
        #Init CSV headers
        if self.debug:
            self.csvFields = ["sentence","lemma","form","lexique","id-phrase","id-lexique","file","category"]
        else:
            self.csvFields = ["id-annotation","id-lexique","category"]

        self.writer = csv.DictWriter(sys.stdout, fieldnames=self.csvFields)

    #Re-encode text (because python 2.7)
    def reEncode(self, text):
        txt = []
        for item in text:
            item = item.encode(self.encoding) if type(item) == unicode else item
            txt += [item]
        return txt

    #Delete preposition in sentence's last position
    def prepInLastPosition(self, lexicalEntry):
        args = lexicalEntry['arguments']
        lexicalText = lexicalEntry["text"]

        if lexicalText is None or len(args) < 2:
            return lexicalText

        #Encoding
    
        lexicalText = self.reEncode(lexicalText)

        for key in args:
            if 'word' not in args[key]:
                continue
            if 'pos' not in args[key]:
                continue

            word = args[key]['word']
            pos = args[key]['pos']

            #Encoding
            if type(pos) == unicode:
                pos = pos.encode(self.encoding)
            if type(word) == unicode:
                word = word.encode(self.encoding)
        
            if lexicalText[-1] == word:
                if pos.lower() == "prép":
                    return lexicalText[:-1]

        return lexicalText

    #Delete determinants 
    def avoidDetDifferences(self, args, lexicalText):
        if lexicalText is None:
            return lexicalText

        typesList = ["det", "loc"]
        listDet = ["Poss0", "Poss1", "DNum"]

        lexicalText = self.reEncode(lexicalText)
        
        for w in lexicalText:
            if w in listDet:
                lexicalText.remove(w)

        for key in args:
            if "pos" in args[key]:
                word = args[key]["pos"] if "word" not in args[key] else args[key]["word"]
            else:
                word = "" if "word" not in args[key] else args[key]["word"]

            if "word" in args[key]:
                pos = args[key]["word"] if "pos" not in args[key] else args[key]["pos"]
            else:
                pos = "" if "pos" not in args[key] else args[key]["pos"]

            #Encoding
            if type(word) == unicode:
                word = word.encode(self.encoding)
            if type(pos) == unicode:
                pos = pos.encode(self.encoding)

            if word in lexicalText:
                if pos.lower() in typesList or word.lower() in typesList:
                    lexicalText.remove(word)
        return lexicalText

  #Get sentence
    def getSentence(self, text):
        return  " ".join(text).replace('"','').strip()
    
  #Exacts Matches
    def getExactMatching(self, lemma, form, lexicalText):
        if lexicalText is None:
            return False

        if abs(len(lexicalText) - len(lemma)) != 0 or abs(len(lexicalText) - len(form)) != 0:
            return False

            #Encoding
            lexicalText = self.reEncode(lexicalText)
            lemma = self.reEncode(lemma)
            form = self.reEncode(form)

        #For "avoir avoir" cases or "bourrelets" cases
        isDifferent = False
        if len(set(lexicalText)) > 1:
            isDifferent = True
            
        if isDifferent:
            #If no lemma, compare with form
            if lemma is None or "_" in lemma:
                if len(list(set(lexicalText) - set(form))) == 0:
                    return True
                return False

            if len(list(set(lexicalText) - set(lemma))) == 0:
                return True
            return False

        if len(lexicalText) == 1:
            if lexicalText[0] not in form and lexicalText[0] not in lemma:
                return False
            return True

        if lemma is None or "_" in lemma:
            for word in form:
                if word in lexicalText:
                    return False
            return True

        for word in lemma:
            if word not in lexicalText:
                return False
        return True

  #Various Matches
  # 'variante'
    def getVariousMatching(self, lemma, form, lexicalText):
        if lemma is None or form is None or lexicalText is None :
            return False

        #If sizes are too much different
        if abs(len(set(lexicalText)) - len(lemma)) > 1 or abs(len(set(lexicalText)) - len(form)) > 1:
                return False

        #For "avoir avoir" cases or "bourrelets" cases
        isDifferent = False
        if len(set(lexicalText)) > 1:
            isDifferent = True

        lexicalText = self.reEncode(lexicalText)
        lemma = self.reEncode(lemma)
        form = self.reEncode(form)

        if not isDifferent:
            ratio = fuzz.token_sort_ratio(" ".join(form), " ".join(lexicalText)) if "_" in lemma else fuzz.token_sort_ratio(" ".join(lemma), " ".join(lexicalText))
            if ratio < 75:
                return False

        for item in lexicalText:
            if item not in lemma and item not in form:
                return False
        return True

  #Semantic matches
  #By arguments
  # 'similaire'
    def getSemanticMatching(self, lemma, form, lexicalEntry, lexicalText):
        lexicalText = self.reEncode(lexicalText)
        if abs(len(lexicalText) - len(lemma)) > 1:
            return False
        ratio = fuzz.token_sort_ratio(" ".join(form), " ".join(lexicalText)) if "_" in lemma else fuzz.token_sort_ratio(" ".join(lemma), " ".join(lexicalText))
        if ratio < 73 :
            return False
        
        ratio2 = fuzz.ratio(" ".join(form), " ".join(lexicalText)) if "_" in lemma else fuzz.ratio(" ".join(lemma), " ".join(lexicalText))
        if ratio2 < 75:
            return False
        # Find arguments
        posPriority = ['C', 'V', 'ADV', 'ADJ']
        byPos = {}

        args = lexicalEntry['arguments']

        for key in args:
            if 'word' not in args[key]:
                return False
            if 'pos' not in args[key]:
                return False

            word = args[key]['word']
            pos = args[key]['pos']

            #Encoding
            if type(word) == unicode:
                word = word.encode(self.encoding)
            if type(pos) == unicode:
                pos = pos.encode(self.encoding)

            #If no word or no pos return false
            if type(word) != str or type(pos) != str:
                return False

            pos = pos.upper()

            if pos not in byPos:
                byPos[pos] = [word]
            else:
                byPos[pos] += [word]

        for currentPos in posPriority:
            if currentPos not in byPos:
                return False

            for word in byPos[currentPos]:
                if word.lower() not in lemma and word.lower() not in form:
                    return False
            return True

        return False

    def alignment(self):

        totalAnnotations = 0
        totalAlignments = 0
        totalAlignedSentences = 0
        totalSentences = len(self.parserTsv.dict_sentences)
        totalLexicalEntries = 0

        for head, lexicalEntries in self.XMLText.items():
            totalLexicalEntries += len(lexicalEntries)

        self.writer.writeheader()
        
        #In corpus dict
        for sentenceId, sentence in self.parserTsv.dict_sentences.items():
            annotations = sentence['annotations']
            sentenceText = self.getSentence(sentence['sentence'])
            
            totalAnnotations += len(annotations)
            nbAlignedAnnotations = 0

            for annotId in annotations:
                annotationMatching = { "exact": [], "variante": [], "similaire": [] }
                annotation = annotations[annotId]

                form = annotation["form"]
                lemma = annotation["lemma"]

                hasMatchingEntry = False
                lexicalEntries = {}
                
                for head in annotation["heads"]:
                    if head in self.XMLText:
                        for entryId, lexicalEntry in self.XMLText[head].items():
                            lexicalEntries[entryId] = lexicalEntry

                #In XML dict
                for lexicalEntry in lexicalEntries.values():
                    lexicalText = self.prepInLastPosition(lexicalEntry)

                    # If expressions are the same
                    if self.getExactMatching(lemma, form, lexicalText):
                        hasMatchingEntry = True
                        annotationMatching["exact"] += [lexicalEntry]
                        continue

                    # #Delete determinants                
                    lexicalText = self.avoidDetDifferences(lexicalEntry["arguments"], lexicalText)
            
                    if self.getVariousMatching(lemma, form, lexicalText):
                        hasMatchingEntry = True
                        annotationMatching["variante"] += [lexicalEntry]
                        continue

                    if not self.getSemanticMatching(lemma, form, lexicalEntry, lexicalText):
                        continue
                    
                    #Alignment by head or by arguments here
                    hasMatchingEntry = True
                    annotationMatching["similaire"] += [lexicalEntry]

                sentenceText = "".join(sentenceText).replace(",","")
                lemma = " ".join(lemma)
                form = " ".join(form)

                #If no matching
                if not hasMatchingEntry:
                    if self.debug:
                        #Debugging
                        self.writer.writerow({
                            'sentence': sentenceText,
                            'lemma': lemma,
                            'form': form,
                            'id-phrase': str(sentenceId)
                        })
                    else:
                        #Production
                        self.writer.writerow({
                            'id-annotation': str(annotation['id'])
                    })
                    continue

                for alignmentType, entries in annotationMatching.items():
                    if len(entries) > 0:
                        totalAlignments += len(entries)
                        nbAlignedAnnotations += 1
                    
                    for entry in entries:
                        #Debugging
                        if self.debug : 
                            sourceFile = entry['source_file']
                            text = " ".join(entry['text'])
                            if type(sourceFile) == unicode:
                                sourceFile = sourceFile.encode(self.encoding)
                            if type(text) == unicode:
                                text = text.encode(self.encoding)

                            self.writer.writerow({
                                'sentence': sentenceText,
                                'lemma': lemma,
                                'form': form,
                                'lexique': text,
                                'id-phrase': str(sentenceId),
                                'id-lexique': str(entry['id']),
                                'file': sourceFile,
                                'category': alignmentType
                            })
                        
                        #Production
                        else :
                            self.writer.writerow({
                                'id-annotation':  str(annotation['id']),
                                'id-lexique': str(entry['id']),
                                'category': alignmentType
                            })
                        
            # end annotations loop
            if nbAlignedAnnotations > 0:
                totalAlignedSentences += 1

        # end sentences loop       
        if self.debug :
            results = {
            "Longueur du corpus": totalSentences,
            "Nb phrases alignees": totalAlignedSentences,
            "Nb d'annotations": totalAnnotations,
            "Nb alignements": totalAlignments,
            "Longueur lexique": totalLexicalEntries
            }
            for key, value in results.items():
                self.writer.writerow({
                    'sentence': key,
                    'lemma':value
                })


def main():
    parser = argparse.ArgumentParser(description='Create alignments between lexical entries and a corpus.')
    parser.add_argument('--debug', dest='debug', help='run in debug mode', action='store_true')

    args = parser.parse_args()

    if bool(args.debug):
        prog = Align(True)
        prog.alignment()
    else:
        prog = Align()
        prog.alignment()
    
if __name__ == "__main__":
    main()

